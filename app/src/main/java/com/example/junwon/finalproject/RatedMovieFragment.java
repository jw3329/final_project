package com.example.junwon.finalproject;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Jun Won on 2017-11-22.
 */

public class RatedMovieFragment extends Fragment {

    View view;

    /**
     * Rated Movie fragment with putting arguments combining MovieListFragment
     *
     * @return returning a view of its fragment
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.collection_container_fragment,container,false);
        getActivity().setTitle("Rated Movies");
        MainActivity.deleteMenu = true;
        Fragment ratedfragment = new MovieListFragment();
        Bundle bundle = new Bundle();
        bundle.putString("flag","rated");
        ratedfragment.setArguments(bundle);
        getFragmentManager().beginTransaction().replace(R.id.collection_container,ratedfragment).commit();
        return view;
    }
}
