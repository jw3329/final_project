package com.example.junwon.finalproject;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by Jun Won on 2017-11-21.
 */

public class MyWatchlistFragment extends Fragment {

    View view;

    /**
     * Watchlist fragment with putting arguments combining MovieListFragment
     *
     * @return returning a view of its fragment
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.collection_container_fragment, container, false);
        getActivity().setTitle("My Watchlist");
        MainActivity.deleteMenu = true;
        Fragment watchlistfragment = new MovieListFragment();
        Bundle bundle = new Bundle();
        bundle.putString("flag", "my_watchlist");
        watchlistfragment.setArguments(bundle);
        getFragmentManager().beginTransaction().replace(R.id.collection_container, watchlistfragment).commit();
        return view;
    }
}
