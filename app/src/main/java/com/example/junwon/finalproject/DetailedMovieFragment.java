package com.example.junwon.finalproject;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import me.grantland.widget.AutofitTextView;

/**
 * Created by Jun Won on 2017-11-21.
 */

public class DetailedMovieFragment extends Fragment {

    View view;
    TextView tagline_text;
    AutofitTextView production_companies;
    int movie_id;
    ImageView banner_image;
    ImageView main_poster;
    RatingBar rating_bar;
    AutofitTextView detailed_title;
    AutofitTextView detailed_genre;
    AutofitTextView overview;
    Bitmap banner_bitmap;
    Bitmap poster_bitmap;
    ArrayList<String> genre_array = new ArrayList<>();
    String movie_title;
    ArrayList<String> production_company_array = new ArrayList<>();
    String release_date;
    String tagline;
    String overview_string;
    int vote_average;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.detailed_movie_fragment, container, false);
        setHasOptionsMenu(true);
        MainActivity.deleteMenu = false;
        setVariables();
        getActivity().setTitle("Movie information");
        getMovieId();
        getData();
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.detailed_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_rate:
                final AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
                View mView = getActivity().getLayoutInflater().inflate(R.layout.menu_ratingbar, null);
                RatingBar rating_bar = (RatingBar) mView.findViewById(R.id.menu_ratingbar);
                final Button rate_button = (Button) mView.findViewById(R.id.rate_button);
                final Button cancel_button = (Button) mView.findViewById(R.id.cancel_button);

                mBuilder.setView(mView);
                final AlertDialog dialog = mBuilder.create();
                rating_bar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                    @Override
                    public void onRatingChanged(RatingBar ratingBar, final float rating, boolean fromUser) {
                        rate_button.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                AsyncTask asynctask = new AsyncTask() {
                                    @Override
                                    protected void onPostExecute(Object o) {
                                        dialog.dismiss();
                                        Toast.makeText(getActivity(), "Successfully Rated",
                                                Toast.LENGTH_LONG).show();
                                    }

                                    @Override
                                    protected Object doInBackground(Object[] params) {
                                        Map<String, Float> map = new HashMap<>();
                                        map.put("value", 2 * rating);
                                        String body = HttpRequest.post("https://api.themoviedb.org/3/movie/" + movie_id + "/rating", true, "api_key",
                                                MainActivity.getApi_key(), "session_id", MainActivity.getSession_id()).form(map).body();
                                        System.out.println(body);
                                        return null;
                                    }
                                }.execute();
                            }
                        });
                        cancel_button.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });
                    }
                });
                dialog.show();
                return true;
            case R.id.menu_delete_rate:
                AsyncTask asynctask = new AsyncTask() {
                    @Override
                    protected void onPostExecute(Object o) {
                        Toast.makeText(getActivity(), "Successfully Deleted",
                                Toast.LENGTH_LONG).show();                    }

                    @Override
                    protected Object doInBackground(Object[] params) {
                        HttpRequest.delete("https://api.themoviedb.org/3/movie/" + movie_id + "/rating", true, "api_key",
                                MainActivity.getApi_key(), "session_id", MainActivity.getSession_id());
                        return null;
                    }
                }.execute();
                return true;
            case R.id.menu_favorite:
                new AsyncTask() {
                    @Override
                    protected void onPostExecute(Object o) {
                        Toast.makeText(getActivity(), "Added to Favorite",
                                Toast.LENGTH_LONG).show();
                    }

                    @Override
                    protected Object doInBackground(Object[] params) {
                        Map<String, Object> map = new HashMap<>();
                        map.put("media_type", "movie");
                        map.put("media_id", movie_id);
                        map.put("favorite", true);
                        String body = HttpRequest.post("https://api.themoviedb.org/3/account/{account_id}/favorite", true, "api_key",
                                MainActivity.getApi_key(), "session_id", MainActivity.getSession_id()).form(map).body();
                        System.out.println(body);
                        return null;
                    }
                }.execute();
                return true;
            case R.id.menu_watchlist:
                new AsyncTask() {
                    @Override
                    protected void onPostExecute(Object o) {
                        Toast.makeText(getActivity(), "Added to My Watchlist",
                                Toast.LENGTH_LONG).show();
                    }

                    @Override
                    protected Object doInBackground(Object[] params) {
                        Map<String, Object> map = new HashMap<>();
                        map.put("media_type", "movie");
                        map.put("media_id", movie_id);
                        map.put("watchlist", true);
                        String body = HttpRequest.post("https://api.themoviedb.org/3/account/{account_id}/watchlist", true, "api_key",
                                MainActivity.getApi_key(), "session_id", MainActivity.getSession_id()).form(map).body();
                        System.out.println(body);
                        return null;
                    }
                }.execute();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Getting movie ID from argument
     */
    private void getMovieId() {
        Bundle bundle = getArguments();
        movie_id = bundle.getInt("movie_id");
    }

    /**
     * Getting data using AsyncTask
     */

    private void getData() {
        AsyncTask asynctask = new AsyncTask() {
            @Override
            protected void onPostExecute(Object o) {
                banner_image.setImageBitmap(banner_bitmap);
                banner_image.setScaleType(ImageView.ScaleType.FIT_XY);
                tagline_text.setText(tagline);
                main_poster.setImageBitmap(poster_bitmap);
                setPosterVideo(main_poster);
                rating_bar.setRating(vote_average);
                String year = release_date.split("-")[0];
                detailed_title.setText(movie_title + " (" + year + ")");
                String genre_string = "";
                for (int i = 0; i < genre_array.size(); i++) {
                    genre_string += genre_array.get(i);
                    if (i != genre_array.size() - 1) {
                        genre_string += ", ";
                    }
                }
                detailed_genre.setText(genre_string);
                String production_string = "";
                for (int i = 0; i < production_company_array.size(); i++) {
                    production_string += production_company_array.get(i);
                    if (i != production_company_array.size() - 1) {
                        production_string += ", ";
                    }
                }
                production_companies.setText(production_string);
                overview.setText(overview_string);
                Bundle bundle = new Bundle();
                bundle.putString("flag", "detailed_recommended");
                bundle.putInt("movie_id", movie_id);
                Fragment fragment = new MovieListFragment();
                fragment.setArguments(bundle);
                getFragmentManager().beginTransaction().add(R.id.detailed_recommended_frame, fragment).commit();
            }

            @Override
            protected Object doInBackground(Object[] params) {
                try {
                    JSONObject jsonObject = new JSONObject(HttpRequest.get("https://api.themoviedb.org/3/movie/" + movie_id,
                            true, "api_key", MainActivity.getApi_key()).body());

                    String link = jsonObject.getString("backdrop_path");
                    String actual_link = "http://image.tmdb.org/t/p/w185" + link;
                    banner_bitmap = getBitmap(actual_link);
                    link = jsonObject.getString("poster_path");
                    actual_link = "http://image.tmdb.org/t/p/w185" + link;
                    poster_bitmap = getBitmap(actual_link);
                    JSONArray genre_jsonarray = jsonObject.getJSONArray("genres");
                    for (int i = 0; i < genre_jsonarray.length(); i++) {
                        JSONObject genre_jsonobject = genre_jsonarray.getJSONObject(i);
                        genre_array.add(genre_jsonobject.getString("name"));
                    }
                    JSONArray production_jsonarray = jsonObject.getJSONArray("production_companies");
                    for (int i = 0; i < production_jsonarray.length(); i++) {
                        JSONObject production_jsonobject = production_jsonarray.getJSONObject(i);
                        production_company_array.add(production_jsonobject.getString("name"));
                    }
                    tagline = jsonObject.getString("tagline");
                    movie_title = jsonObject.getString("title");
                    release_date = jsonObject.getString("release_date");
                    vote_average = jsonObject.getInt("vote_average");
                    overview_string = jsonObject.getString("overview");
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }

    /**
     * Clicking poster will lead to youtube video
     *
     * @param poster the Image of poster
     */
    private void setPosterVideo(ImageView poster) {
        poster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AsyncTask() {
                    String video_link;

                    @Override
                    protected void onPostExecute(Object o) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(video_link)));
                        Log.d("video", "Video playing!!");
                    }

                    @Override
                    protected Object doInBackground(Object[] params) {
                        try {
                            JSONObject jsonObject = new JSONObject(HttpRequest.get("https://api.themoviedb.org/3/movie/"
                                    + movie_id + "/videos", true, "api_key", MainActivity.getApi_key()).body());
                            JSONArray jsonArray = jsonObject.getJSONArray("results");
                            JSONObject first_jsonObject = jsonArray.getJSONObject(0);
                            String key = first_jsonObject.getString("key");
                            video_link = "https://www.youtube.com/watch?v=" + key;
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                }.execute();
            }
        });
    }

    /**
     * get bitmap image from the link indicated
     *
     * @param actual_link link to get the image
     * @return bitmap image
     * @throws IOException exception for IO
     */
    private Bitmap getBitmap(String actual_link) throws IOException {
        URL urlConnection = new URL(actual_link);
        HttpURLConnection connection = (HttpURLConnection) urlConnection.openConnection();
        connection.connect();
        InputStream input = connection.getInputStream();
        return BitmapFactory.decodeStream(input);
    }

    /**
     * Setting up all variables in this fragment class
     */

    private void setVariables() {
        banner_image = (ImageView) view.findViewById(R.id.banner_image);
        main_poster = (ImageView) view.findViewById(R.id.main_poster);
        rating_bar = (RatingBar) view.findViewById(R.id.rating_bar);
        detailed_title = (AutofitTextView) view.findViewById(R.id.detail_title);
        detailed_genre = (AutofitTextView) view.findViewById(R.id.detail_genre);
        overview = (AutofitTextView) view.findViewById(R.id.overview);
        tagline_text = (TextView) view.findViewById(R.id.tagline);
        production_companies = (AutofitTextView) view.findViewById(R.id.production_companies);
    }

}
