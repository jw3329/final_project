package com.example.junwon.finalproject;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Jun Won on 2017-11-21.
 */

public class MovieListFragment extends Fragment {
    View view;
    ArrayList<Bitmap> poster = new ArrayList<>();
    ArrayList<String> title = new ArrayList<>();
    ArrayList<Integer> vote_average = new ArrayList<>();
    ArrayList<ArrayList<String>> genre_array = new ArrayList<>();
    ArrayList<String> released_date = new ArrayList<>();
    ListView collection_listview;
    ArrayList<Integer> movie_id = new ArrayList<>();
    String flag;
    customAdapter adapter;
    ArrayList<Integer> watchlist_movie_id = new ArrayList<>();
    ArrayList<Integer> favorite_movie_id = new ArrayList<>();
    ArrayList<Boolean> watchlist_array = new ArrayList<>();
    ArrayList<Boolean> favorite_array = new ArrayList<>();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.collection_fragment, container, false);
        Bundle bundle = getArguments();
        flag = bundle.getString("flag");
        collection_listview = (ListView) view.findViewById(R.id.collection_listview);
        getMyListSetUp();
        setHasOptionsMenu(true);
        return view;
    }

    /**
     * ListView Setting up function
     */
    private void getMyListSetUp() {
        new AsyncTask() {

            @Override
            protected void onPostExecute(Object o) {
                obtainData();
            }

            @Override
            protected Object doInBackground(Object[] params) {
                try {
                    JSONObject FavoriteJsonObject = new JSONObject(HttpRequest.get("https://api.themoviedb.org/3/account/{account_id}/favorite/movies",
                            true, "api_key", MainActivity.getApi_key(), "session_id", MainActivity.getSession_id(),
                            "sort_by", "created_at.desc").body());

                    JSONArray FavoriteResult = FavoriteJsonObject.getJSONArray("results");
                    for (int i = 0; i < FavoriteResult.length(); i++) {
                        JSONObject jsonObject = FavoriteResult.getJSONObject(i);
                        favorite_movie_id.add(jsonObject.getInt("id"));
                    }

                    JSONObject WatchJsonObject = new JSONObject(HttpRequest.get("https://api.themoviedb.org/3/account/{account_id}/watchlist/movies",
                            true, "api_key", MainActivity.getApi_key(), "session_id", MainActivity.getSession_id(),
                            "sort_by", "created_at.desc").body());

                    JSONArray WatchResult = WatchJsonObject.getJSONArray("results");
                    for (int i = 0; i < WatchResult.length(); i++) {
                        JSONObject jsonObject = WatchResult.getJSONObject(i);
                        watchlist_movie_id.add(jsonObject.getInt("id"));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }

    /**
     * Obtaining data for ListView
     */

    private void obtainData() {
        AsyncTask asyncTask = new AsyncTask() {
            @Override
            protected void onPostExecute(Object o) {
                adapter = new customAdapter();
                collection_listview.setAdapter(adapter);

                collection_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Bundle bundle = new Bundle();
                        bundle.putInt("movie_id", movie_id.get(position));
                        Fragment fragment = new DetailedMovieFragment();
                        fragment.setArguments(bundle);
                        getFragmentManager().beginTransaction().replace(R.id.main_frame, fragment).addToBackStack(null).commit();
                    }
                });
            }

            @Override
            protected Object doInBackground(Object[] params) {

                try {
                    JSONObject jsonObject = null;
                    if (flag.equals("upcoming")) {
                        jsonObject = new JSONObject(HttpRequest.get("https://api.themoviedb.org/3/movie/upcoming",
                                true, "api_key", MainActivity.getApi_key()).body());
                    } else if (flag.equals("my_watchlist")) {
                        jsonObject = new JSONObject(HttpRequest.get("https://api.themoviedb.org/3/account/{account_id}/watchlist/movies",
                                true, "api_key", MainActivity.getApi_key(), "session_id", MainActivity.getSession_id(),
                                "sort_by", "created_at.desc").body());
                    } else if (flag.equals("detailed_recommended")) {
                        Bundle bundle = getArguments();
                        int detailed_movie_id = bundle.getInt("movie_id");
                        jsonObject = new JSONObject(HttpRequest.get("https://api.themoviedb.org/3/movie/" + detailed_movie_id + "/recommendations",
                                true, "api_key", MainActivity.getApi_key()).body());
                    } else if (flag.equals("rated")) {
                        jsonObject = new JSONObject(HttpRequest.get("https://api.themoviedb.org/3/account/{account_id}/rated/movies",
                                true, "api_key", MainActivity.getApi_key(), "session_id", MainActivity.getSession_id(),
                                "sort_by", "created_at.desc").body());
                    } else if (flag.equals("search")) {
                        Bundle bundle = getArguments();
                        String text = bundle.getString("text");
                        jsonObject = new JSONObject(HttpRequest.get("https://api.themoviedb.org/3/search/movie",
                                true, "api_key", MainActivity.getApi_key(), "query", text).body());
                    }
                    JSONArray result_array = jsonObject.getJSONArray("results");
                    for (int i = 0; i < result_array.length(); i++) {
                        JSONObject result_object = result_array.getJSONObject(i);
                        String link = result_object.getString("poster_path");
                        String actual_link = "http://image.tmdb.org/t/p/w185" + link;
                        Bitmap myBitmap = getBitmap(actual_link);
                        poster.add(myBitmap);
                        movie_id.add(result_object.getInt("id"));
                        int det_id = result_object.getInt("id");
                        setWatchAndFavoriteArray(det_id);
                        title.add(result_object.getString("title"));
                        vote_average.add(result_object.getInt("vote_average"));
                        released_date.add(result_object.getString("release_date"));
                        JSONArray genre = result_object.getJSONArray("genre_ids");
                        ArrayList<String> inner_genre_array = new ArrayList<>();
                        for (int j = 0; j < genre.length(); j++) {
                            int id = genre.getInt(j);
                            Map<Integer, String> map = ((MainActivity) getActivity()).genre_map;
                            inner_genre_array.add(map.get(id));
                            genre_array.add(inner_genre_array);
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }

    /**
     * Setting up boolean ArrayList to judge if it is in watchlist or favorite
     *
     * @param id Movie id
     */

    private void setWatchAndFavoriteArray(int id) {
        boolean isWatchList = false;
        for (int j : watchlist_movie_id) {
            if (id == j) {
                isWatchList = true;
                break;
            }
        }
        watchlist_array.add(isWatchList);
        boolean isFavorite = false;
        for (int j : favorite_movie_id) {
            if (id == j) {
                isFavorite = true;
                break;
            }
        }
        favorite_array.add(isFavorite);
    }

    /**
     * get bitmap image from the link indicated
     *
     * @param actual_link link to get the image
     * @return bitmap image
     * @throws IOException exception for IO
     */

    private Bitmap getBitmap(String actual_link) throws IOException {
        URL urlConnection = new URL(actual_link);
        HttpURLConnection connection = (HttpURLConnection) urlConnection.openConnection();
        connection.connect();
        InputStream input = connection.getInputStream();
        return BitmapFactory.decodeStream(input);
    }

    /**
     * Private class of the custom adapter of ListView
     */

    private class customAdapter extends BaseAdapter implements Filterable {

        ArrayList<Bitmap> m_poster;
        ArrayList<String> m_title = new ArrayList<>();
        ArrayList<Integer> m_vote_average = new ArrayList<>();
        ArrayList<ArrayList<String>> m_genre_array = new ArrayList<>();
        ArrayList<String> m_released_date = new ArrayList<>();
        ArrayList<Integer> m_movie_id = new ArrayList<>();

        /**
         * Constructor of CustomAdapter
         */
        public customAdapter() {
            this.m_poster = poster;
            this.m_title = title;
            this.m_vote_average = vote_average;
            this.m_genre_array = genre_array;
            this.m_released_date = released_date;
            this.m_movie_id = movie_id;
        }

        @Override
        public int getCount() {
            return m_poster.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = getActivity().getLayoutInflater().inflate(R.layout.custom_listview, null);
            ImageView movie_poster = (ImageView) convertView.findViewById(R.id.movie_poster);
            TextView movie_title = (TextView) convertView.findViewById(R.id.movie_title);
            TextView release_date = (TextView) convertView.findViewById(R.id.release_date);
            TextView vote_average_text = (TextView) convertView.findViewById(R.id.vote_average);
            TextView genres = (TextView) convertView.findViewById(R.id.genres);
            ImageView watchlist_button = (ImageView) convertView.findViewById(R.id.watchlist_button);
            ImageView favorite_button = (ImageView) convertView.findViewById(R.id.favorite_button);

            movie_poster.setImageBitmap(m_poster.get(position));
            movie_title.setText(m_title.get(position));
            release_date.setText(m_released_date.get(position));
            vote_average_text.setText(String.valueOf("Average rated: " + m_vote_average.get(position)));
            ArrayList<String> array = m_genre_array.get(position);
            String string = "";
            for (int i = 0; i < array.size(); i++) {
                string += array.get(i);
                if (i != array.size() - 1) {
                    string += ", ";
                }
            }
            genres.setText(string);
            boolean isWatch = watchlist_array.get(position);
            boolean isFavorite = favorite_array.get(position);
            int movieId = movie_id.get(position);

            updateIcon(watchlist_button, favorite_button, isWatch, isFavorite);
            setWatchlistandFavoriteButtonListener(watchlist_button, favorite_button, isWatch, isFavorite, movieId, position);
            return convertView;
        }

        /**
         * Setting up filtering function
         * @return  Filter returning value
         */
        @Override
        public Filter getFilter() {
            return new Filter() {
                ArrayList<Integer> index_list = new ArrayList<>();
                int flag = 0;

                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    constraint = constraint.toString().toLowerCase();
                    FilterResults results = new FilterResults();

                    if (constraint != null && constraint.length() > 0) {
                        ArrayList<String> filtered = new ArrayList<>();
                        for (int i = 0; i < title.size(); i++) {
                            if (title.get(i).toString().toLowerCase().contains(constraint)) {
                                filtered.add(title.get(i));
                                index_list.add(i);
                            }
                        }

                        results.values = filtered;
                        results.count = filtered.size();
                        flag = 1;
                    } else {
                        results.values = title;
                        results.count = title.size();
                        flag = 2;
                    }
                    return results;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    ArrayList<Bitmap> new_poster = new ArrayList<>();
                    ArrayList<String> new_title = new ArrayList<>();
                    ArrayList<Integer> new_vote_average = new ArrayList<>();
                    ArrayList<ArrayList<String>> new_genre_array = new ArrayList<>();
                    ArrayList<String> new_released_date = new ArrayList<>();
                    ArrayList<Integer> new_movie_id = new ArrayList<>();
                    for (int i = 0; i < index_list.size(); i++) {
                        new_poster.add(poster.get(index_list.get(i)));
                        new_title.add(title.get(index_list.get(i)));
                        new_vote_average.add(vote_average.get(index_list.get(i)));
                        new_genre_array.add(genre_array.get(index_list.get(i)));
                        new_released_date.add(released_date.get(index_list.get(i)));
                        new_movie_id.add(movie_id.get(index_list.get(i)));
                    }
                    if (flag == 1) {
                        m_poster = new_poster;
                        m_title = new_title;
                        m_vote_average = new_vote_average;
                        m_genre_array = new_genre_array;
                        m_released_date = new_released_date;
                        m_movie_id = new_movie_id;
                    } else if (flag == 2) {
                        m_poster = poster;
                        m_title = title;
                        m_vote_average = vote_average;
                        m_genre_array = genre_array;
                        m_released_date = released_date;
                        m_movie_id = movie_id;
                    }
                    notifyDataSetChanged();
                }
            };
        }
    }

    /**
     * Update icon by getting 4 variables which is needed to judge
     * @param watchlist_button  watchlist button to update
     * @param favorite_button   favorite button to update
     * @param isWatch   boolean watch variable to judge
     * @param isFavorite    boolean favorite variable to judge
     */
    private void updateIcon(ImageView watchlist_button, ImageView favorite_button, boolean isWatch, boolean isFavorite) {
        if (isWatch) {
            watchlist_button.setImageResource(android.R.drawable.btn_star_big_on);
        } else {
            watchlist_button.setImageResource(android.R.drawable.btn_star_big_off);
        }

        if (isFavorite) {
            favorite_button.setImageResource(R.drawable.ic_favorite_black_24dp);
        } else {
            favorite_button.setImageResource(R.drawable.ic_favorite_border_black_24dp);
        }
    }

    /**
     * Setting up the listener of when either watchlist or favorite image is clicked
     * @param watchlist_button  watchlist button to update
     * @param favorite_button   favorite button to update
     * @param isWatch   boolean watch variable to judge
     * @param isFavorite    boolean favorite variable to judge
     * @param movieId   movie id
     * @param position  position of the custom ListView
     */

    private void setWatchlistandFavoriteButtonListener(final ImageView watchlist_button, final ImageView favorite_button,
                                                       final boolean isWatch, final boolean isFavorite, final int movieId, final int position) {
        watchlist_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncTask() {
                    @Override
                    protected void onPostExecute(Object o) {
                        updateIcon(watchlist_button, favorite_button, watchlist_array.get(position), favorite_array.get(position));
                    }

                    @Override
                    protected Object doInBackground(Object[] params) {
                        if (watchlist_array.get(position)) {
                            Map<String, Object> map = new HashMap<>();
                            map.put("media_type", "movie");
                            map.put("media_id", movieId);
                            map.put("watchlist", false);
                            String body = HttpRequest.post("https://api.themoviedb.org/3/account/{account_id}/watchlist", true, "api_key",
                                    MainActivity.getApi_key(), "session_id", MainActivity.getSession_id()).form(map).body();
                        } else {
                            Map<String, Object> map = new HashMap<>();
                            map.put("media_type", "movie");
                            map.put("media_id", movieId);
                            map.put("watchlist", true);
                            String body = HttpRequest.post("https://api.themoviedb.org/3/account/{account_id}/watchlist", true, "api_key",
                                    MainActivity.getApi_key(), "session_id", MainActivity.getSession_id()).form(map).body();
                        }
                        watchlist_array.set(position, !watchlist_array.get(position));
                        return null;
                    }
                }.execute();
            }
        });
        favorite_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncTask() {

                    @Override
                    protected void onPostExecute(Object o) {
                        updateIcon(watchlist_button, favorite_button, watchlist_array.get(position), favorite_array.get(position));
                    }

                    @Override
                    protected Object doInBackground(Object[] params) {
                        if (favorite_array.get(position)) {
                            Map<String, Object> map = new HashMap<>();
                            map.put("media_type", "movie");
                            map.put("media_id", movieId);
                            map.put("favorite", false);
                            String body = HttpRequest.post("https://api.themoviedb.org/3/account/{account_id}/favorite", true, "api_key",
                                    MainActivity.getApi_key(), "session_id", MainActivity.getSession_id()).form(map).body();
                        } else {
                            Map<String, Object> map = new HashMap<>();
                            map.put("media_type", "movie");
                            map.put("media_id", movieId);
                            map.put("favorite", true);
                            String body = HttpRequest.post("https://api.themoviedb.org/3/account/{account_id}/favorite", true, "api_key",
                                    MainActivity.getApi_key(), "session_id", MainActivity.getSession_id()).form(map).body();
                        }
                        favorite_array.set(position, !favorite_array.get(position));
                        return null;
                    }
                }.execute();
            }
        });
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (MainActivity.deleteMenu) {
            menu.clear();
        }
        inflater.inflate(R.menu.search_menu, menu);
        MenuItem item = menu.findItem(R.id.menuSearch);
        SearchView search_view = (SearchView) item.getActionView();
        search_view.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });
    }

}
