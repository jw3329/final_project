package com.example.junwon.finalproject;

import android.app.Fragment;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

/**
 * Created by Jun Won on 2017-11-27.
 */

public class SearchMovieFragment extends Fragment {

    View view;
    EditText search_text;
    ImageButton search_button;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.search_movie_fragment, container, false);
        getActivity().setTitle("Search Movies");
        MainActivity.deleteMenu = true;
        SetUpVariables();
        SetButtonClick();
        return view;
    }

    /**
     * Setting up button click when clicking for search
     */
    private void SetButtonClick() {
        search_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = search_text.getText().toString();
                changeFragment(text);
            }
        });
    }

    /**
     * replacing fragment when searching
     *
     * @param text the keyword query to search
     */
    private void changeFragment(String text) {
        Bundle bundle = new Bundle();
        bundle.putString("flag", "search");
        bundle.putString("text", text);
        Fragment fragment = new MovieListFragment();
        fragment.setArguments(bundle);
        getFragmentManager().beginTransaction().replace(R.id.search_container, fragment).commit();
    }

    /**
     * Setting up all of variables
     */

    private void SetUpVariables() {
        search_text = (EditText) view.findViewById(R.id.search_text);
        search_button = (ImageButton) view.findViewById(R.id.search_button);
    }
}
