package com.example.junwon.finalproject;

import android.content.Intent;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {

    public String api_key = "7a3ab68737e6c2d44b1c27c3d1b90f0a";
    public String session_id = "e61370e64733ae8adad24c5772016227b799667c";
    @Test
    public void responseTest() {
        int response = HttpRequest.get("http://google.com").code();
        System.out.println(response);
    }

    @Test
    public void responseBodyTest() {
        String response = HttpRequest.get("http://google.com").body();
        System.out.println("Response was: " + response);
    }

    @Test
    public void recieveTest() {
        HttpRequest.get("http://google.com").receive(System.out);
    }

    @Test
    public void addingParameterTest() {
        HttpRequest request = HttpRequest.get("http://google.com", true, 'q', "baseball gloves", "size", 100);
        System.out.println(request.toString());
    }

    @Test
    public void getAccountInfoTest() {
        HttpRequest request = HttpRequest.get("https://api.themoviedb.org/3/account", true, "api_key", api_key, "session_id", session_id);
        System.out.println(request.code());
        System.out.println(request.body());
    }

    @Test
    public void debuggingTest() throws JSONException, UnirestException {
        Map<String,Float> map = new HashMap<>();
        map.put("value", (float) 8.5);
        String body = HttpRequest.post("https://api.themoviedb.org/3/movie/141052/rating",true,"api_key",api_key,"session_id",session_id).form(map).body();
        System.out.println(body);
    }

    @Test
    public void seconddebuggingTest() throws JSONException, UnirestException {
        Map<String,Object> map = new HashMap<>();
        map.put("media_type","movie");
        map.put("media_id",440021);
        map.put("favorite",true);
        String body = HttpRequest.post("https://api.themoviedb.org/3/account/{account_id}/favorite",true,"api_key",
                MainActivity.getApi_key(),"session_id",MainActivity.getSession_id()).form(map).body();
        System.out.println(body);
    }


}